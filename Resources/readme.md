# Resources
Last updated : 13/2/2025 - more "positif" blocks.

# Blacklist (self-sourced)
## Apple stuff Android shouldn't be loading (whitelist this if you use Apple device)
* appleid.cdn-apple.com

## Fate/GO NA - Unwanted connections
* backtrace.io
* events.backtrace.io
* api.reproio.com
* launches.appflyer.com
* collect.analytics.unity3d.com
* pls.prd.mz.internal.unity3d.com
* app.adjust.world
* app.adjust.net.in

## Genshin Impact - Unwanted connections
* log-upload-os.hoyoverse.com
* ad-log-upload-os.hoyoverse.com
* apm-log-upload-os.hoyoverse.com
* sdk-log-upload-os.hoyoverse.com
* ys-log-upload-os.hoyoverse.com
* minor-api-os.hoyoverse.com
* osuspider.yuanshen.com

## More AppsFlyer crap
* mmusbg-cdn-settings.appsflyersdk.com
* mmusbg-conversions.appsflyersdk.com
* mmusbg-gcdsdk.appsflyersdk.com
* mmusbg-inapps.appsflyersdk.com
* mmusbg-launches.appsflyersdk.com

## Some weird stuff from some islands between Asia & Australia (totally not some vengeful entry made by some nobody who got cockblocked off neocities somewhere there)
* ads.indosatooredoo.com
* internet-positif.org
* www.internet-positif.org
* trustpositif.kominfo.go.id
* uzone.co.id
* redirect.uzone.co.id
* uzone.id
* digital.uzone.id
* games.uzone.id
* mercusuar.uzone.id
* tools.uzone.id
* www.uzone.id

# Whitelist
* DigDeeper's clearnet sites
* my clearnet sites
* wrongthink.link

# Redirect
* None for now...

# Other debatable stuff (not in blacklist yet, but may enter depending on personal judgment)
## Geetest (Chinese captcha)
* geetest.com
* api-na.geetest.com
* static.geetest.com
* www.geetest.com
